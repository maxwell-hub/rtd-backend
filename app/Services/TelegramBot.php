<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class TelegramBot
{
    protected $httpClient;

    protected $channelId;

    private $botToken;

    public function __construct()
    {
        $this->botToken = config('app.telegram.token');

        $this->httpClient = new Client();

        $this->channelId = config('app.telegram.channel_id');
    }

    public function sendMessage(string $message)
    {
        try {
            if (!$this->botToken) {
                throw new Exception('Telegram bot token is not set');
            }

            $this->httpClient->post("https://api.telegram.org/bot{$this->botToken}/sendMessage", [
                'json' => [
                    'chat_id' => $this->channelId,
                    'text' => $message,
                ],
            ]);
        } catch (Exception $e) {
            Log::info('Telegram bot error', [$e->getMessage()]);
        }
    }
}
