<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;

class GeoIP
{
    const ENDPOINT = 'http://ip-api.com/json/%s?fields=';

    public $fields = [
        'status', 'message', 'country', 'city'
    ];

    protected $client;

    public static function make(): GeoIP
    {
        return new static();
    }

    public static function getIp(): ?string
    {
        return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR']: null;
    }

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getGeolocation(string $ip, array $fields = []): array
    {
        $requestFields = implode(',', array_merge($this->fields, $fields));

        try {
            $response = $this->client->get(
                sprintf(self::ENDPOINT, $ip, $requestFields)
            );

            return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
        } catch (Exception $e) {
        }

        return [];
    }
}
