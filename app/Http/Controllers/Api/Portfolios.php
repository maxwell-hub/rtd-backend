<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PortfolioResource;
use App\Portfolio;
use Illuminate\Http\Resources\Json\JsonResource;

class Portfolios
{
    public function __invoke(): JsonResource
    {
        return PortfolioResource::collection(
            Portfolio::query()
                ->with('media')
                ->ordered()
                ->simplePaginate(100)
        );
    }
}
