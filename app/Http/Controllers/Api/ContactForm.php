<?php

namespace App\Http\Controllers\Api;

use App\Services\GeoIP;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Services\TelegramBot;

class ContactForm extends BaseApiController
{
    public $rules = [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255',
        'message' => 'nullable|string|max:2000',
    ];

    public function __invoke(Request $request)
    {
        $formData = $this->getRequestData($request);

        $this->validateRequest($formData);

        $this->notifyOwners($formData);

        return $this->successResponse(null, 201);
    }

    public function notifyOwners(array $formData): void
    {
        $country = null;

        if (GeoIP::getIp()) {
            $detectedLocation = GeoIP::make()->getGeolocation(GeoIP::getIp());

            $country = Arr::get($detectedLocation, 'country') . ' / ' . Arr::get($detectedLocation, 'city');
        }

        $message = view('telegram-message', [
            'name' => Arr::get($formData, 'name'),
            'email' => Arr::get($formData, 'email'),
            'message' => Arr::get($formData, 'message'),
            'country' => $country,
        ])->render();

        (new TelegramBot())->sendMessage($message);
    }
}
