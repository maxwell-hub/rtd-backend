<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

abstract class BaseApiController
{
    public $rules = [];

    public function validateRequest(array $data): array
    {
        $validator = Validator::make($data, $this->rules);

        return $validator->validate();
    }

    public function filterRequestData(array $data): array
    {
        return array_intersect_key($data, $this->rules);
    }

    public function getRequestData(Request $request): array
    {
        return $this->filterRequestData($request->all());
    }

    public function badResponse(string $message, array $errors = [], $status = 400): JsonResponse
    {
        return response()->json(
            array_merge(['message' => $message], $errors),
            $status
        );
    }

    public function successResponse(string $message = null, $status = 200): JsonResponse
    {
        return response()->json([
            'message' => $message ?? 'Request accepted',
        ], $status);
    }
}
