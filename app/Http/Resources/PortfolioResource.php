<?php

namespace App\Http\Resources;

use App\Portfolio;
use Illuminate\Http\Resources\Json\JsonResource;

class PortfolioResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'keywords' => $this->keywords,
            'meta_description' => $this->meta_description,

            'project_name' => $this->project_name,
            'description' => $this->getDescription(),
            'preview_url' => $this->preview_url,

            'cover' => optional($this->getFirstMedia(Portfolio::MEDIA_COLLECTION_COVER))->getFullUrl(),
        ];
    }
}
