<?php

namespace App;

use Illuminate\Mail\Markdown;
use Ofcold\NovaSortable\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Portfolio extends Model implements HasMedia
{
    use HasMediaTrait;
    use SortableTrait;

    const MEDIA_COLLECTION_COVER = 'cover';

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::MEDIA_COLLECTION_COVER)->singleFile();
    }

    public function getDescription()
    {
        return Markdown::parse($this->description)->toHtml();
    }
}
