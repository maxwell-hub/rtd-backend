<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');

            $table->string('locale', 2)->default('en');

            $table->unsignedInteger('sort_order')->nullable();

            $table->string('title')->nullable();

            $table->string('meta_description', 300)->nullable();

            $table->string('keywords', 300)->nullable();

            $table->string('project_name');

            $table->text('description')->nullable();

            $table->string('preview_url')->nullable();

            $table->boolean('visible')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
