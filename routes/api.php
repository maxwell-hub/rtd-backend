<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware(['api'])->group(function () {

    Route::get('test-route', function () {
        return response()->json([
            'now' => date('Y-m-d H:i:s'),
            'base_url' => request()->fullUrl(),
            'server_info' => [
                'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
                'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'],
                'HTTP_X_FORWARDED_FOR' => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR']: null,
            ]
        ]);
    });


    Route::get('portfolios', 'Api\Portfolios');
    Route::post('send-contact-form', 'Api\ContactForm');
});
